import * as yup from 'yup'

export const employeeSchema = yup.object().shape({
    NamaLengkap:yup.string().required('Nama lengkap is required'),
    TanggalLahir:yup.date().required('Tanggal lahir is required'),
    Umur:yup.number().required('Umur is required'),
    AlamatLengkap:yup.string().required('Alamat lengkap is required'),
    Kd_Provinsi:yup.string().required('Provinsi is required'),
    Kd_Kota:yup.string().required('Kota is required'),
    Sertifikasi: yup.array()
    .of(
      yup.object().shape({
        NamaLembaga: yup.string(),
        Id_Bidang: yup.string(),
        DokumenSertifikat: yup.object(),
      })
    ).required('Sertifikasi is required').min(1, 'At least 1 Sertifikasi'),
})