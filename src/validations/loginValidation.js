import * as yup from 'yup'

export const loginSchema = yup.object().shape({
    Email:yup.string().required('Email is required'),
    Password:yup.string().required('Password is required').min(8, 'Password length must be at least 8 characters'),
})