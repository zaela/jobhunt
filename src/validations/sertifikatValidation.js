import * as yup from 'yup'

export const sertifikatSchema = yup.object().shape({
    NamaLembaga:yup.string().required('Nama lembaga is required'),
    Id_Bidang:yup.string().required('Bidang is required'),
    DokumenSertifikat:yup.object().required('Dokumen sertifikat is required'),
})