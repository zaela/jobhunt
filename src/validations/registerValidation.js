import * as yup from 'yup'

export const registerSchema = yup.object().shape({
    Username:yup.string().required('Username is required'),
    Email:yup.string().required('Email is required'),
    Password:yup.string().required('Password is required').min(8, 'Password length must be at least 8 characters'),
})