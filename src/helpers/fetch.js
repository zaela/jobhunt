const baseURL = new URL('https://klinikme-test-api.herokuapp.com/api/v1/')

function handleResponse(response) {
  let contentType = response.headers.get('content-type')
  if (contentType.includes('application/json')) {
    return handleJSONResponse(response)
  } else if (contentType.includes('text/html')) {
    return handleTextResponse(response)
  } else {
    throw new Error(`Sorry, content-type ${contentType} not supported`)
  }
}

function handleJSONResponse(response) {
  return response.json().then((json) => {
    if (response.ok) {
      return json
    } else {
      if (json.errorCode === 'UNAUTHORIZED_USER') {
        localStorage.removeItem('google_token')
        localStorage.removeItem('email')
        localStorage.removeItem('name')
        localStorage.removeItem('token')
        localStorage.removeItem('user')
      }
      return Promise.reject(
        Object.assign({}, json, {
          status: response.status,
          statusText: response.statusText,
        })
      )
    }
  })
}

function handleTextResponse(response) {
  return response.text().then((text) => {
    if (response.ok) {
      return text
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText,
        err: text,
      })
    }
  })
}

export async function fetchPost(url, body) {
  return fetch(new URL(url, baseURL), {
    method: 'POST',
    headers: {
      'Content-type': 'application/json',
    },
    body: JSON.stringify(body),
  }).then(handleResponse)
}

export async function fetchGet(url, params) {
  return fetch(new URL(url, baseURL), {
    method: 'GET',
    headers: {
      'Content-type': 'application/json',
    },
    params:params
  }).then(handleResponse)
}

export async function fetchAuthPost(url, body, method = 'POST') {
  return fetch(new URL(url, baseURL), {
    method: method,
    headers: {
      'Content-type': 'application/json',
      'x-auth-token': localStorage.token,
      'Authorization': `Bearer ${localStorage.token}`,
    },
    body: JSON.stringify(body),
  }).then(handleResponse)
}

export async function fetchAuthPostFormData(url, formData, method = 'POST') {
  return fetch(new URL(url, baseURL), {
    method: method,
    headers: {
      'x-auth-token': localStorage.token,
    },
    body: formData,
  }).then(handleResponse)
}

export async function fetchAuthGet(url) {
  return fetch(new URL(url, baseURL), {
    method: 'GET',
    headers: {
      'Content-type': 'application/json',
      'x-auth-token': localStorage.token,
    },
  }).then(handleResponse)
}

export async function fetchOptionalGet(url) {
  const headers = {
    'Content-type': 'application/json',
  }

  if (localStorage.token) {
    headers['x-auth-token'] = localStorage.token
  }

  return fetch(new URL(url, baseURL), {
    method: 'GET',
    headers,
  }).then(handleResponse)
}
