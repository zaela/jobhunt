export function isAuthenticated() {
  const user = authenticatedUser()
  if (typeof window !== 'undefined') {
    return Boolean(localStorage.token)
  }
}

export function authenticatedUser() {
  if (typeof window !== 'undefined' && localStorage.user) {
    return JSON.parse(localStorage.user)
  }

  return {}
}
