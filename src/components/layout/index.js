import Head from 'next/head'
import Image from 'next/image'
import Header from '../header'


export default function Layout({children, page}) {
  return (
    <main className='min-h-screen w-full bg-white'>
        <Header page={page} />
        {children}
    </main>
  )
}
