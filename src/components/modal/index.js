import { Dialog, Transition } from '@headlessui/react'
import { RxCross2 } from 'react-icons/rx';
import { TbTrash } from 'react-icons/tb';
import { Fragment, useState } from 'react'

export default function Modal({isOpen, handleCloseModal, handleSubmit, handleReset, children}) {
    return (
        <Transition appear show={isOpen} as={Fragment}>
            <Dialog as="div" className="relative z-10" 
                onClose={() => handleCloseModal()}
            >
                <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
                >
                <div className="fixed inset-0 bg-black bg-opacity-25" />
                </Transition.Child>

                <div className="fixed inset-0 overflow-y-auto">
                <div className="flex min-h-full items-center justify-center p-4 text-center">
                    <Transition.Child
                    as={Fragment}
                    enter="ease-out duration-300"
                    enterFrom="opacity-0 scale-95"
                    enterTo="opacity-100 scale-100"
                    leave="ease-in duration-200"
                    leaveFrom="opacity-100 scale-100"
                    leaveTo="opacity-0 scale-95"
                    >
                    <Dialog.Panel className="w-full max-w-lg transform overflow-hidden rounded-lg bg-white p-5 text-left align-middle shadow-xl transition-all">
                        <Dialog.Title
                            as="h3"
                            className="flex justify-between"
                        >
                            <span className='text-lg font-medium leading-6 text-gray-600'>Sertifikasi</span>
                            <RxCross2 className='w-4 h-4 cursor-pointer' onClick={() => handleCloseModal(false)}/>
                        </Dialog.Title>
                        <div className="mt-2">
                            {children}
                        </div>

                        <div className="mt-4 flex justify-between">
                            {
                                handleReset && (
                                    <div className='flex gap-1 items-center w-fit cursor-pointer' onClick={() => handleReset()}>
                                        <TbTrash className='text-sm text-red-500 w-[1.22rem] h-[1.22rem]'/>
                                        <span className='text-sm text-gray-500'>Hapus</span>
                                    </div>
                                )
                            }
                            <button
                                type="button"
                                className="inline-flex justify-center rounded-md border border-transparent bg-primary px-4 py-1.5 text-sm font-medium text-white hover:bg-blue-400 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                onClick={() => handleSubmit()}
                            >
                                Simpan
                            </button>
                        </div>
                    </Dialog.Panel>
                    </Transition.Child>
                </div>
                </div>
            </Dialog>
        </Transition>
    )
}
