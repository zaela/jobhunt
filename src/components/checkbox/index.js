export default function Checkbox({label, name, value, classInput, handleChange, error}) {
    return (
        <div className="form-control flex items-center">
            <input 
                type="checkbox" className='checked:bg-primary w-4 h-4' 
                checked={value} 
                name={name}
                onChange={(e) => handleChange(e.target.checked, name)}
            />
            <span className="label-text text-sm text-gray-500 ml-1">{label}</span> 
        </div>
    )
}