import AsyncSelect from 'react-select/async';


export default function AsyncSelectInput({label, value, data, classSelect, onChange, loadOptions, error, touched}) {
  const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' }
  ]
  return (
    <div className={classSelect}>
        <label className="text-sm text-gray-500">{label}</label>
        <AsyncSelect  
          className="focus:ring-primary focus:border-primary"
          cacheOptions
          value={value}
          defaultOptions={data}
          loadOptions={loadOptions}
          getOptionValue={(e) => e.value}
          getOptionLabel={(e) => e.label}
          noOptionsMessage={() => "Data not found"}
          onChange={(e) => onChange(e)}
        />
        {
            error && touched && <p className='text-xs mt-1 w-full text-red-600'>{error}</p>
        }
    </div>
  )
}
