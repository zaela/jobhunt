import { isAuthenticated } from '@/helpers/isAuthenticated'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'


export default function Header({page}) {
  const router = useRouter()
  const [isLogin, setIsLogin] = useState(false)

  const logOut = () => {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    router.push('/login')
  }

  useEffect(() => {
    setIsLogin(isAuthenticated())
  }, [])
  
  return (
    <nav className='w-full flex justify-between py-3 fixed top-0 px-6 sm:py-4 sm:px-14 items-center bg-white z-50'>
        <img src='/assets/jobhuntLogo.svg' className='w-36'/>
        {
            page && (
                <p className='text-gray-700 text-lg hidden sm:block'>{page}</p>
            )
        }

        {
          isLogin ? 
          <button className='border border-primary rounded-lg text-primary py-1 px-3' onClick={() => logOut()}>Keluar</button>
          :
          <Link href={'/login'} className='border border-primary rounded-lg text-primary py-1 px-3'>Masuk</Link>
        }
    </nav>
  )
}
