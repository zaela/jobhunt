import Swal from 'sweetalert2'

export const SwalError = ({title, message}) => {
  return (
    Swal.fire({
        icon: 'error',
        title: title,
        text: message,
        footer:`<div className="h-10"></div>`,
        customClass:{
          icon: 'bg-red-200 border border-red-200 text-green-600 border-none no-border',
          footer:'swal-height',
        },
        showCancelButton: false,
        showConfirmButton: false
      })
  )
}
