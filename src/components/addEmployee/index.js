import Checkbox from '@/components/checkbox'
import Input from '@/components/input'
import Layout from '@/components/layout'
import ModalAddSertifikat from '@/components/modalAddSertifikat'
import { fetchGet } from '@/helpers/fetch'
import { isAuthenticated } from '@/helpers/isAuthenticated'
import { employeeSchema } from '@/validations/employeeValidation'
import { useFormik } from 'formik'
import moment from 'moment/moment'
import { Inter } from 'next/font/google'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { BsPlusLg } from "react-icons/bs"
import { FiMinus } from "react-icons/fi"
import { RxCross2 } from 'react-icons/rx'
import Select from 'react-select'
import AsyncSelect from 'react-select/async'
import Swal from 'sweetalert2'

const inter = Inter({ subsets: ['latin'] })

export default function AddEmployeeComponent({handleAdd}) {
  const router = useRouter()
  const [personalInfo, setPersonalInfo] = useState(false)
  const [sertification, setSertification] = useState(false)
  const [isOpen, setIsOpen] = useState(false)
  const [dataProvince, setDataProvince] = useState([])
  const [dataKota, setDataKota] = useState([])
  const [dataBidang, setDataBidang] = useState([])
  const [province, setProvince] = useState([])
  const [kota, setKota] = useState([])
  const [bidang, setBidang] = useState('')

  const getProvince = async inputValue => {
    const data = await fetchGet(`data_provinsi${inputValue ? `?provinsi=${inputValue}` : ''}`)
    if (data.status === "success") {
      setDataProvince(
        data.data
      )

      return data.data
    }
  }

  const getKota = async inputValue => {
    const data = await fetchGet(`data_kota${inputValue ? `?provinsi=${inputValue}` : ''}`)
    if (data.status === "success") {
      const cities = data.data.map((city) => {
        return {
          label:city.NamaKota,
          value:city.Kd_Kota,
        }
      })
      setDataKota(cities)

      return cities
    }
  }

  const getBidang = async inputValue => {
    const data = await fetchGet(`data_bidang`)
    if (data.status === "success") {
      setDataBidang(
        data.data
      )

      return data.data
    }
  }

  useEffect(() => {
    if (!isAuthenticated()) {
      router.push('/login')
    }
    getProvince()
    getKota()
    getBidang()
  },[])
  
  const closeModal = () => {
    setIsOpen(false)
  }

  const openModal = () => {
    setIsOpen(true)
  }

  const onSubmit = async (values, actions) => {
    handleAdd(values)
  }

  const { values, errors, touched, setFieldValue, handleBlur, handleReset, handleChange, handleSubmit} = useFormik({
      initialValues:{
        NamaLengkap: "",
        TanggalLahir: "",
        Umur: "",
        AlamatLengkap: "",
        Kd_Provinsi: "",
        Kd_Kota: "",
        Keahlian: [],
        LevelPekerjaan: "",
        Sertifikasi: [],
      },
      validationSchema:employeeSchema,
      onSubmit,
  })
  
  console.log('values add employee:', values)
  console.log('errors add employee:', values)

  const handleAge = (birth) => {
    const initialZero = (value) => {
      if (value.slice(0,1) == 0) {
        return value.slice(1,2)
      }
      return value
    }
   
    const year = moment().format('YYYY')
    const birthYear = moment(birth).format('YYYY')
    const month = initialZero(moment().format('MM'))
    const birthMonth = initialZero(moment(birth).format('MM'))
    const date = initialZero(moment().format('DD'))
    const birthDate = initialZero(moment(birth).format('DD'))
    const isBirthday = (month > birthMonth || (month >= birthMonth && date >= birthDate)) ? 0 : 1
    
    const age = year - birthYear - isBirthday
    setFieldValue('Umur', age)
  }

  return (
    <form className='w-full' onSubmit={handleSubmit}>
        <div>
            <div className='flex justify-between items-center p-5'>
                <p className={`${personalInfo ? 'text-primary' : 'text-gray-600'} text-[1.04rem]`}>Informasi Pribadi</p>
                {
                personalInfo ?
                    <FiMinus className={`${personalInfo ? 'text-primary' : 'text-gray-600'} w-4.5 h-4.5`} onClick={() => setPersonalInfo(!personalInfo)}/>
                :
                    <BsPlusLg className='w-4.5 h-4.5 text-gray-600' onClick={() => setPersonalInfo(!personalInfo)}/>
                }
            </div>
            <div className={`w-full ${personalInfo ? '' : 'hidden'} px-5`}>
            <Input label="Nama Lengkap" name="NamaLengkap" classInput="mb-3"
                type="text" value={values.NamaLengkap} handleChange={(e) => handleChange(e)} 
                error={errors.NamaLengkap}
                touched={touched.NamaLengkap}
            />
            <Input label="Tanggal Lahir" name="TanggalLahir" classInput="mb-3"
                type="date" value={values.TanggalLahir} 
                handleChange={(e) => {
                handleChange(e)
                handleAge(e.target.value)
                }}
                error={errors.TanggalLahir}
                touched={touched.TanggalLahir}
            />
            <Input label="Umur" name="Umur" classInput="mb-3"
                type="number" value={values.Umur}
                disabled
            />
            <Input label="Alamat Lengkap" name="AlamatLengkap" classInput="mb-3"
                type="text" value={values.AlamatLengkap} handleChange={(e) => handleChange(e)} 
                error={errors.AlamatLengkap}
                touched={touched.AlamatLengkap}
            />
            
            <div>
                <label className="text-sm text-gray-500">Provinsi</label>
                <AsyncSelect  
                className="focus:ring-primary focus:border-primary"
                cacheOptions
                value={province}
                defaultOptions={dataProvince}
                loadOptions={getProvince}
                getOptionValue={(e) => e.Kd_Provinsi}
                getOptionLabel={(e) => e.NamaProvinsi}
                noOptionsMessage={() => "Data not found"}
                onChange={(e) => {
                    setFieldValue('Kd_Provinsi', e.Kd_Provinsi)
                    setProvince(e)
                    setFieldValue('Kd_Kota', '')
                    setKota('')
                    getKota(e.Kd_Provinsi)
                }}
                />
                {
                    errors.Kd_Provinsi && touched.Kd_Provinsi && <p className='text-xs mt-1 w-full text-red-600'>{errors.Kd_Provinsi}</p>
                }
            </div>
            <div className='h-3'/>
            <div>
                <label className="text-sm text-gray-500">Kota</label>
                <Select 
                    className="focus:ring-primary focus:border-primary"
                    name="Kd_Kota"
                    value={kota}
                    options={dataKota}
                    noOptionsMessage={() => "Data not found"}
                    onChange={(e) => {
                        setFieldValue('Kd_Kota', e.value)
                        setKota(e)
                    }}
                />
                {
                    errors.Kd_Kota && touched.Kd_Kota && <p className='text-xs mt-1 w-full text-red-600'>{errors.Kd_Kota}</p>
                }
            </div>
            <div className='mt-3'>
                <p className='text-sm text-gray-500'>Keahlian</p>
                <p className='text-xs text-gray-400'>Pilih keahlian / soft skill yang anda miliki</p>
                <div className='mt-2 flex flex-wrap justify-between gap-8'>
                {
                    dataBidang.length > 0 && dataBidang.map((bidang, index) => {
                    return ( 
                        <Checkbox label={bidang?.NamaBidang} 
                        value={bidang?.NamaBidang === values.Keahlian[index] ? true : false}
                        name={bidang?.NamaBidang} 
                        handleChange={(isChecked, name) => {
                            if (isChecked) {
                            let keahlian = values.Keahlian
                            if (keahlian.length >= (index+1)) {
                                keahlian[index]=name
                            } else {
                                keahlian.push(name)
                            }
                            setFieldValue('Keahlian', keahlian)
                            } else {
                            let keahlian = values.Keahlian
                            keahlian[index]=''
                            setFieldValue('Keahlian', keahlian)
                            }
                        }}
                        />
                    )
                    })
                }
                </div>
            </div>
            <div className='mt-4'>
                <p className='text-sm text-gray-500'>Level Pekerjaan</p>
                <p className='text-xs text-gray-400'>Pilih level pekerjaan yang sesuai dengan pengalaman dan profeciency yang anda miliki</p>
                <div className='mt-2 flex flex-col gap-2'>
                    <Checkbox label='Entry Level'
                    name="Entry Level"
                    value={values.LevelPekerjaan === "Entry Level" ? true : false}
                    handleChange={(isChecked, name) => isChecked ? setFieldValue('LevelPekerjaan', name) : setFieldValue('LevelPekerjaan', '')} 
                    />
                    <Checkbox label='Middle Level'
                    name="Middle Level"
                    value={values.LevelPekerjaan === "Middle Level" ? true : false}
                    handleChange={(isChecked, name) => isChecked ? setFieldValue('LevelPekerjaan', name) : setFieldValue('LevelPekerjaan', '')} 
                    />
                    <Checkbox label='Senior Level'
                    name="Senior Level"
                    value={values.LevelPekerjaan === "Senior Level" ? true : false}
                    handleChange={(isChecked, name) => isChecked ? setFieldValue('LevelPekerjaan', name) : setFieldValue('LevelPekerjaan', '')} 
                    />

                </div>
            </div>
            </div>
            <div className='flex justify-between items-center p-5'>
                <p className={`${sertification ? 'text-primary' : 'text-gray-600'} text-[1.04rem]`}>Sertifikasi</p>
                {
                sertification ?
                    <FiMinus className={`${sertification ? 'text-primary' : 'text-gray-600'} w-4.5 h-4.5`} onClick={() => setSertification(!sertification)}/>
                :
                    <BsPlusLg className='w-4.5 h-4.5 text-gray-600' onClick={() => setSertification(!sertification)}/>
                }
            </div>
            <div className='px-5'>
                {
                    errors.Sertifikasi && touched.Sertifikasi && <p className='text-xs mt-1 w-full text-red-600'>{errors.Sertifikasi}</p>
                }
            </div>
            <div className={`w-full ${sertification ? '' : 'hidden'} px-5 mt-2`}>
                <div className='w-full flex fle-wrap gap-4 pb-6'>
                    {
                    values.Sertifikasi.length > 0 &&  values.Sertifikasi.map((sertifikat) => {
                    return(
                        <div className='w-8 h-8 relative'>
                            <div className='cursor-pointer text-red-500 -right-1 absolute -top-1 bg-white rounded-full p-[0.2rem]'>
                                <RxCross2 className='w-2 h-2'/>
                            </div>
                            <img src={`${sertifikat?.DokumenSertifikat?.url ? sertifikat?.DokumenSertifikat?.url : '/assets/defaultImage.svg'}`} alt="sertifikat" className='w-full'/>
                        </div>
                    )
                    })
                    }
                </div>
                <button type='button' className='px-2.5 py-1 rounded-sm' style={{boxShadow:"0px 1px 2px 0.2px #979393"}}
                    onClick={() => openModal()}
                >
                    <div className='flex gap-1 items-center'>
                        <BsPlusLg className='w-4 h-4 text-primary'/>
                        <span className='text-sm text-primary'>Tambah sertifikasi</span>
                    </div>
                </button>
            </div>
            <div className={`w-full flex justify-end px-5 mt-2 mb-8`}>
                <button
                    type="submit"
                    className="w-fit flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-primary disabled:bg-blue-300 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
                >
                    Simpan
                </button>
            </div>
            <ModalAddSertifikat isOpen={isOpen} closeModal={() => closeModal()} dataBidang={dataBidang} 
                addSertifikat={(sertifikat) => {
                    let data = values.Sertifikasi
                    data.push(sertifikat)
                    setFieldValue('Sertifikasi', data)
                }} 
            />
        </div>
    </form>
  )
}
