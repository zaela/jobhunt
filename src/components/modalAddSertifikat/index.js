import Input from '@/components/input'
import Modal from '@/components/modal'
import { employeeSchema } from '@/validations/employeeValidation'
import { sertifikatSchema } from '@/validations/sertifikatValidation'
import { useFormik } from 'formik'
import { Inter } from 'next/font/google'
import { useRef, useState } from 'react'
import Select from 'react-select'
import Swal from 'sweetalert2'

const inter = Inter({ subsets: ['latin'] })

export default function ModalAddSertifikat({isOpen, closeModal, dataBidang, addSertifikat}) {
  const [sertification, setSertification] = useState('')
  const [bidang, setBidang] = useState('')

  const fileInput = useRef();
  const selectFile = () => {
      fileInput.current.click();
  }

  const onSubmit = async (values, actions) => {
    addSertifikat(values)
    handleReset()
    setBidang('')
    setSertification('')
    closeModal()
  }



  const { values, errors, touched, setFieldValue, handleBlur, handleReset, handleChange, handleSubmit} = useFormik({
      initialValues:{
        NamaLembaga : "",
        Id_Bidang : "",
        DokumenSertifikat : ""
    },
      validationSchema:sertifikatSchema,
      onSubmit,
  })

  console.log('values add sertifikat:', values)
  console.log('errors add sertifikat:', values)
  
  const uploadFoto = ( file, size, url) => {
    if (file) {
      if (size > 625000) {
        Swal.fire({
          // iconHtml: `<h1>hahah</h1>`,
          icon: 'error',
          title: 'Gagal',
          text: 'Maksimal ukuran file 5 Mb',
          footer:`<div className="h-10"></div>`,
          customClass:{
            icon: 'bg-red-200 border border-red-200 text-green-600 border-none no-border',
            footer:'swal-height',
          },
          // timer: 3000,
          showCancelButton: false,
          showConfirmButton: false
        })
      } else {
        let formData = new FormData()
        formData.append('image', file)
        let sertifikat = {
            formData:formData,
            url:url
        }
        setSertification(sertifikat)
        setFieldValue('DokumenSertifikat', sertifikat)
      }
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Gagal',
        text: 'Silahkan coba lagi',
        footer:`<div className="h-10"></div>`,
        customClass:{
          icon: 'bg-red-200 border border-red-200 text-green-600 border-none no-border',
          footer:'swal-height',
        },
        showCancelButton: false,
        showConfirmButton: false
      })
    }
  }

  return (
    <Modal
        isOpen={isOpen}
        handleCloseModal={() => closeModal()}
        handleSubmit={() => handleSubmit()}
        handleReset={() =>  {
            handleReset()
            setBidang('')
            setSertification('')
        }}
    >
        <Input label="Nama Lembaga" name="NamaLembaga" classInput="mb-3"
            type="text" value={values.NamaLembaga} handleChange={(e) => handleChange(e)} 
            error={errors.NamaLembaga}
            touched={touched.NamaLembaga}
        />
        
        <div>
            <label className="text-sm text-gray-500">Bidang</label>
            <Select 
                className="focus:ring-primary focus:border-primary"
                name="Id_Bidang"
                value={bidang}
                options={dataBidang}
                getOptionValue={(e) => e.Id_Bidang}
                getOptionLabel={(e) => e.NamaBidang}
                noOptionsMessage={() => "Data not found"}
                onChange={(e) => {
                    setFieldValue('Id_Bidang', e.Id_Bidang)
                    setBidang(e)
                }}
            />
            {
                errors.Id_Bidang && touched.Id_Bidang && <p className='text-xs mt-1 w-full text-red-600'>{errors.Id_Bidang}</p>
            }
        </div>

        <div className='h-3'/>
        <div>
            <label className="text-sm text-gray-500">Unggah Sertifikat</label>
            
            <div className='w-full p-4 rounded-md border border-gray-300 flex justify-center'>
                <input type='file' accept={'.png, .jpg, .jpeg'} onChange={(e) => uploadFoto(e.target.files[0], e.target.files[0].size, URL.createObjectURL(e.target.files[0]))} 
                    style={{ "display": "none" }} ref={fileInput}
                />
                <div className='w-fit'>
                <div className='w-full flex justify-center mb-4'>
                    <img src={`${sertification?.url ? sertification?.url : '/assets/defaultImage.svg'}`} alt='sertifikat' className='w-8'/>
                </div>
                <button type='button' onClick={selectFile} className='border border-primary rounded-md text-primary text-xs py-1 px-5'>
                    Pilih Foto
                </button>
                <p className='text-gray-400 text-xs text-center mt-1'>Size max 5MB</p>
                </div>    
            </div>
            {
                errors.DokumenSertifikat && touched.DokumenSertifikat && <p className='text-xs mt-1 w-full text-red-600'>{errors.DokumenSertifikat}</p>
            }
        </div>
    </Modal>
  )
}
