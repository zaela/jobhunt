import Select from 'react-select';


export default function SelectInput({label, name, value, data, classSelect, handleChange, error, touched}) {
  const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' }
  ]
  return (
    <div>
        <label className="text-sm text-gray-500">{label}</label>
        <Select 
          className="focus:ring-primary focus:border-primary"
          name={name}
          value={value}
          options={options}
          noOptionsMessage={() => "Data not found"}
          onChange={(e) => handleChange(e)}
        />
        {
            error && touched && <p className='text-xs mt-1 w-full text-red-600'>{error}</p>
        }
    </div>
  )
}
