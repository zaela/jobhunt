export default function Input({label, placeholder, name, type, value, disabled, classInput, handleChange, error, touched}) {
  return (
    <div className={`${classInput}`}>
        {
          label && (
            <label className="text-sm text-gray-500">{label}</label>
          )
        }
        <input
            id={name}
            name={name}
            type={type}
            autoComplete={name}
            placeholder={placeholder}
            value={value}
            onChange={(event) => handleChange(event)}
            className={`appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm`}
            disabled={disabled}
        />
        {
            error && touched && <p className='text-xs mt-1 w-full text-red-600'>{error}</p>
        }
    </div>
  )
}
