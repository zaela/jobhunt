import Input from "@/components/input";
import { fetchPost } from "@/helpers/fetch";
import { loginSchema } from "@/validations/loginValidation";
import { useFormik } from "formik";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { useMutation } from "react-query";

export default function LoginPage() {
    const router = useRouter()
    const [errorMessage, setErrorMessage] = useState("");

    const { isLoading, mutate, error } = useMutation((user) => fetchPost(`data_user/masuk`, user), {
        onSuccess(response) {
            console.log('response:',response)
            localStorage.token = response.token
            localStorage.user = JSON.stringify(response.data, null, 2)
            router.push('/pegawai/add')
        },
        onError(err) {
            setErrorMessage(err?.message)
        },
    })

    const onSubmit = async (values, actions) => {
        mutate(values)
        // actions.resetForm();
    }

    const { values, errors, touched, handleBlur, handleReset, handleChange, handleSubmit} = useFormik({
        initialValues:{
          Email: "",
          Password: "",
        },
        validationSchema:loginSchema,
        onSubmit,
    })

    return (
        <div className="min-h-screen bg-white">
            <Head>
                <title>JobHunt | Login</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <div className="w-full min-h-screen flex">
                <div className="w-1/2 min-h-screen bg-blue-500 pb-10 flex items-end justify-center" style={{ backgroundImage: "url('/assets/loginBackground.jpg')", backgroundRepeat:"no-repeat", backgroundSize:"cover" }}>
                    <img src="/assets/jobHuntLogo.svg" alt="jobHunt Logo" className="w-60"/>
                </div>
                <div className="w-1/2 h-full my-auto">
                    <div className="sm:mx-auto sm:w-full sm:max-w-md">
                        <div>
                            <h2 className="mt-6 text-center text-2xl font-extrabold text-gray-800">
                            Hallo!
                            </h2>
                            <p className="mt-2 text-center text-sm text-gray-500">
                                Silahkan masukan data dibawah ini
                            </p>
                        </div>

                        <div className="mt-4">
                            <div className="py-8 px-4">
                                <form className="space-y-6" onSubmit={handleSubmit}>
                                    <div className="mt-1">
                                        <Input 
                                            name="Email" classInput="mt-1" placeholder="Email"
                                            type="email" value={values.Email} handleChange={(e) => {
                                                handleChange(e)
                                                setErrorMessage("")
                                            }}
                                            error={errors.Email}
                                            touched={touched.Email}
                                        />
                                        {
                                            errorMessage && <p className='text-xs mt-1 w-full text-red-600'>{errorMessage}</p>
                                        }
                                    </div>
                                    <div className="mt-1">
                                        <Input 
                                            name="Password" classInput="mt-1" placeholder="Password"
                                            type="password" value={values.Password} handleChange={(e) => {
                                                handleChange(e)
                                                setErrorMessage("")
                                            }}
                                            error={errors.Password}
                                            touched={touched.Password}
                                        />
                                        {
                                            errorMessage && <p className='text-xs mt-1 w-full text-red-600'>{errorMessage}</p>
                                        }
                                    </div>
                                    <div>
                                        <button
                                            disabled={isLoading}
                                            type="submit"
                                            className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-primary disabled:bg-blue-300 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
                                        >
                                            Masuk
                                        </button>
                                    </div>
                                    <div>
                                        <p className="text-center text-sm text-gray-500">Belum punya akun? <Link href={'/register'} className="text-primary">Daftar sekarang</Link></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
                
