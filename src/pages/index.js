import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import Layout from '@/components/layout'
import Link from 'next/link'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <div className='min-h-screen w-full bg-white mt-12'>
      <Head>
        <title>JobHunt</title>
        <meta name="description" content="JobHunt" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout>
        <div className='w-full flex flex-col sm:flex sm:flex-row px-8 sm:px-16'>
          <div className='w-full sm:w-1/2 pr-10 sm:min-h-screen flex pt-12 sm:pt-32'>
            <div>
              <div className='sm:w-2/3'>
                <div>
                  <p className='text-3xl text-gray-700 font-semibold'>Temukan Masa Depan Karirmu Sekarang</p>
                  <p className='text-base text-gray-600 mt-2'>Kami membantu menghubungkan para job seeker dengan perusahaan perusahaan terbaik di indonesia</p>
                </div>
              </div>
              <div className='flex gap-6 mt-10'>
                <Link
                    className="bg-primary py-1 px-6 rounded-md flex justify-center items-center"
                    href={'/'}
                >
                    <span className='text-sm text-white'>Selengkapnya</span>
                </Link>
                <Link
                  className="border border-primary py-1 px-2 md:px-4 rounded-md flex justify-center gap-3 items-center"
                  href={'/'}
                >
                  <span className='text-sm text-primary'>Hubungi Kami</span>
                </Link>
              </div>
            </div>
          </div>
          <div className='w-full sm:w-1/2 sm:min-h-screen p-5 pt-12 order-first sm:order-last flex justify-center'>
            <img src='/assets/homeIllustration.svg' className='h-3/4'/>
          </div>
        </div>
      </Layout>
      <div className='fixed right-6 bottom-10'>
        <div className='py-1 pr-8 h-10 rounded-r-full bg-[#CEECFA] flex items-center gap-3 shadow-md'>
          <div className='w-12 h-12 rounded-full bg-white -ml-2.5 shadow-lg p-2.5'>
            <img src='/assets/diamon.svg' alt='premium logo' className='w-full animate-wiggle'/>
          </div>
          <div>
            <p className='text-[0.72rem] text-primary'>Get Premium</p>
            <p className='text-gray-500 text-[0.6rem]'>For easy aplication</p>
          </div>
        </div>
      </div>
    </div>
  )
}
