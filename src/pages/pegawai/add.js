import Checkbox from '@/components/checkbox'
import Input from '@/components/input'
import Layout from '@/components/layout'
import Modal from '@/components/modal'
import SelectInput from '@/components/select'
import { SwalError } from '@/components/swalError'
import { employeeSchema } from '@/validations/employeeValidation'
import { useFormik } from 'formik'
import { Inter } from 'next/font/google'
import Head from 'next/head'
import { useEffect, useRef, useState } from 'react'
import { BsPlusLg } from "react-icons/bs"
import { FiMinus } from "react-icons/fi"
import Swal from 'sweetalert2'
import { RxCross2 } from 'react-icons/rx';
import { useRouter } from 'next/router'
import { isAuthenticated } from '@/helpers/isAuthenticated'
import moment from 'moment/moment'
import { fetchAuthPost, fetchGet } from '@/helpers/fetch'
import { useMutation, useQuery } from 'react-query'
import AsyncSelectInput from '@/components/selectAsync'
import Select from 'react-select'
import AsyncSelect from 'react-select/async';
import { TbTrash } from 'react-icons/tb';
import { ImPencil } from 'react-icons/im';
import ModalAddSertifikat from '@/components/modalAddSertifikat'
import AddEmployeeComponent from '@/components/addEmployee'

const inter = Inter({ subsets: ['latin'] })

export default function AddEmployee() {
  const router = useRouter()
  const [employee, setEmployee] = useState([])
  const [show, setShow] = useState(false)

  useEffect(() => {
    if (!isAuthenticated()) {
      router.push('/login')
    }
  },[])

  const handleAdd = (dataEmployee) => {
    setEmployee([
      ...employee,
      dataEmployee
    ]) 
  }

  const deleteEmployee = (index) => {
    const data = [...employee]
    data.splice(index, 1);
    setEmployee(data) 
  }

  const { isLoading, mutate, error } = useMutation((data) => fetchAuthPost(`daftar_pegawai`, data), {
    onSuccess() {
      Swal.fire({
        icon: 'success',
        title: 'Selamat!',
        text: 'Data berhasil disimpan',
        footer:`<div className="h-10"></div>`,
        customClass:{
          icon: 'bg-blue-200 border border-blue-200 text-primary border-none no-border',
          footer:'swal-height',
        },
        timer: 3000,
        showCancelButton: false,
        showConfirmButton: false
      })
    },
    onError(err) {
      Swal.fire({
        icon: 'error',
        title: 'Gagal',
        text: err.message,
        footer:`<div className="h-10"></div>`,
        customClass:{
          icon: 'bg-red-200 border border-red-200 text-red-400 border-none no-border',
          footer:'swal-height',
        },
        timer: 3000,
        showCancelButton: false,
        showConfirmButton: false
      })
    },
  })

  const onSubmit = async () => {
    const data = employee.map((item) => {
      const settifikat = item.Sertifikasi.map((item) => {
        return {
          ...item,
          // DokumenSertifikat:item.DokumenSertifikat.formData
          DokumenSertifikat:item.DokumenSertifikat.url
        }
      })
      return {
        ...item,
        Umur:String(item.Umur),
        Keahlian:item.Keahlian.join(';'),
        Sertifikasi:settifikat
      }
    })
    mutate({Data:data})
  }

  return (
    <div className='min-h-screen w-full bg-white mt-12'>
      <Head>
        <title>JobHunt | Add Pegawai</title>
        <meta name="description" content="JobHunt" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout page="Data Pegawai">
        <div className='w-full flex justify-center px-8 sm:px-16'>
          <div className='w-full sm:w-1/2 mt-16'>
            {/* list pegawai */}
            {
              !show && employee.length > 0 && employee.map((item, index) => {
                return (
                  <div className='w-full bg-white shadow-lg px-3 py-4 flex justify-between'>
                    <spam className="text-base text-gray-600">{item.NamaLengkap}</spam>
                    <div className='w-fit flex items-center gap-3'>
                      <TbTrash className='text-sm text-red-500 w-[1.22rem] h-[1.22rem]' onClick={() => deleteEmployee(index)}/>
                      <ImPencil className='text-sm text-primary w-[1.22rem] h-[1.22rem]'/>
                    </div>
                  </div>
                )
              })
            }

            {
              !show && (
                <div className='w-full mt-6'>
                  <div className='w-fit flex items-center gap-4 cursor-pointer' onClick={() => setShow(true)}>
                    <div className='p-2 rounded-md border border-gray-300'>
                      <BsPlusLg className='w-4 h-4 text-primary'/>
                    </div>
                    <span className='text-base text-primary'>Tambah Pegawai</span>
                  </div>
                  <div className='mt-2'>
                    <p className='text-gray-400 text-xs'>Tambah jika anda ingin mendaftarkan lebih dari satu pegawai</p>
                  </div>
                </div>
              )
            }

            
            {/* Add employee */}
            {
              show && (
                <AddEmployeeComponent 
                  handleAdd={(dataEmployee) => {
                    handleAdd(dataEmployee)
                    setShow(false)
                  }}
                />
              )
            }

            {/* show button kirim */}
            {
              !show && employee.length > 0 && (
                <button
                    onClick={() => onSubmit()}
                    disabled={isLoading}
                    type="submit"
                    className="w-full mt-8 flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-primary disabled:bg-blue-300 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
                >
                    Kirim
                </button>
              )
            }
          </div>
        </div>
      </Layout>
    </div>
  )
}
