import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import Layout from '@/components/layout'
import Link from 'next/link'

const inter = Inter({ subsets: ['latin'] })

export default function Employee() {
  return (
    <div className='min-h-screen w-full bg-white mt-12'>
      <Head>
        <title>JobHunt | Pegawai</title>
        <meta name="description" content="JobHunt" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout page="Data Pegawai">
        <div className='w-full flex flex-col sm:flex sm:flex-row px-8 sm:px-16'>
          <div className='w-full sm:w-1/2 flex justify-center'>
            <div className='flex'>

            </div>
          </div>
        </div>
      </Layout>
    </div>
  )
}
