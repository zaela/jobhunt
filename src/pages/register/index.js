import Input from "@/components/input";
import { fetchPost } from "@/helpers/fetch";
import { employeeSchema } from "@/validations/employeeValidation";
import { registerSchema } from "@/validations/registerValidation";
import { useFormik } from "formik";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { useMutation } from "react-query";
import Swal from "sweetalert2";

export default function RegisterPage() {
    const router = useRouter()
    const [errorMessage, setErrorMessage] = useState("");

    const { isLoading, mutate, error } = useMutation((user) => fetchPost(`data_user/daftar`, user), {
        onSuccess() {
            Swal.fire({
                icon: 'success',
                title: 'Selamat',
                text: 'Pendaftaran Berhasil',
                footer:`<div className="h-10"></div>`,
                customClass:{
                  icon: 'bg-blue-200 border border-blue-200 text-primary border-none no-border',
                  footer:'swal-height',
                },
                timer: 3000,
                showCancelButton: false,
                showConfirmButton: false
            })
            router.push('/login')
        },
        onError(err) {
            setErrorMessage(err?.message)
        },
    })

    const onSubmit = async (values, actions) => {
        mutate(values)
        // actions.resetForm();
    }

    const { values, errors, touched, handleChange, handleSubmit} = useFormik({
        initialValues:{
          Username: "",
          Email: "",
          Password: "",
        },
        validationSchema:registerSchema,
        onSubmit,
    })

    return (
        <div className="min-h-screen bg-white">
            <Head>
                <title> JobHunt | Register</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <div className="w-full min-h-screen flex">
                <div className="w-1/2 min-h-screen bg-blue-500 pb-10 flex items-end justify-center" style={{ backgroundImage: "url('/assets/registerBackround.jpg')", backgroundRepeat:"no-repeat", backgroundSize:"cover" }}>
                    <img src="/assets/jobHuntLogo.svg" alt="jobHunt Logo" className="w-60"/>
                </div>
                <div className="w-1/2 h-full my-auto">
                    <div className="sm:mx-auto sm:w-full sm:max-w-md">
                        <div>
                            <h2 className="mt-6 text-center text-2xl font-extrabold text-gray-800">
                            Selamat Datang
                            </h2>
                            <p className="mt-2 text-center text-sm text-gray-500">
                                Mulai perjalanan kamu dengan jobHunt.id
                            </p>
                        </div>

                        <div className="mt-4">
                            <div className="py-8 px-4">
                                <form className="space-y-6" onSubmit={handleSubmit}>
                                    <div className="mt-1">
                                        <Input 
                                            name="Username" classInput="mt-1" placeholder="Username"
                                            type="text" value={values.Username} handleChange={(e) => handleChange(e)} 
                                            error={errors.Username}
                                            touched={touched.Username}
                                        />
                                        {
                                            errorMessage && <p className='text-xs mt-1 w-full text-red-600'>{errorMessage}</p>
                                        }
                                    </div>
                                    <div className="mt-1">
                                        <Input 
                                            name="Email" classInput="mt-1" placeholder="Email"
                                            type="email" value={values.Email} handleChange={(e) => handleChange(e)} 
                                            error={errors.Email}
                                            touched={touched.Email}
                                        />
                                        {
                                            errorMessage && <p className='text-xs mt-1 w-full text-red-600'>{errorMessage}</p>
                                        }
                                    </div>
                                    <div className="mt-1">
                                        <Input 
                                            name="Password" classInput="mt-1" placeholder="Password"
                                            type="password" value={values.Password} handleChange={(e) => handleChange(e)} 
                                            error={errors.Password}
                                            touched={touched.Password}
                                        />
                                        {
                                            errorMessage && <p className='text-xs mt-1 w-full text-red-600'>{errorMessage}</p>
                                        }
                                    </div>
                                    <div>
                                        <button
                                            disabled={isLoading}
                                            type="submit"
                                            className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-primary disabled:bg-blue-300 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
                                        >
                                            Daftar
                                        </button>
                                    </div>
                                    <div>
                                        <p className="text-center text-sm text-gray-500">Sudah punya akun? <Link href={'/login'} className="text-primary">Masuk sekarang</Link></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
                
